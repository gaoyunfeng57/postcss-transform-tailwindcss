const fs = require('fs')
const { test } = require('node:test')
const globs = require('globs');


test('does test xchargo', () => {
  globs('../xchargo/src/pages/**/*.scss', function (err, files) {
    if (err) {
      throw err;
    }
    files.forEach((i) => {
      fs.unlinkSync(i)
    })
  });
  globs('../xchargo/src/features/core/components/**/*.scss', function (err, files) {
    if (err) {
      throw err;
    }
    files.forEach((i) => {
      fs.unlinkSync(i)
    })
  });
  globs('../xchargo/src/components/**/*.scss', function (err, files) {
    if (err) {
      throw err;
    }
    files.forEach((i) => {
      fs.unlinkSync(i)
    })
  });
});
