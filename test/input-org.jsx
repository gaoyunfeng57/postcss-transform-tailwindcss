import React from "react";

function Footer() {
  return <div className="footer-btns">
      <div className="btn"></div>
      <div className="footer-btns-content">
        <div className="btn">
          <img className="service-icon" src="https://img3.dian.so/lhc/2023/11/28/132w_132h_753C01701151179.png" />
        </div>
        <div className="scan-btn">
          <img className="btn-icon" src="https://img3.dian.so/lhc/2023/09/06/63w_63h_88CF91693996106.png" />
          <div className="scan-text">扫码充电</div>
        </div>
        <div className="btn">
          <img className={true ?"user-icon": "user-icon-2"} src="https://img3.dian.so/lhc/2023/11/28/132w_132h_4FF241701151209.png" />
          <img className={`${window.a && 'user-icon'} btn`} src="https://img3.dian.so/lhc/2023/11/28/132w_132h_4FF241701151209.png" />
          <img className={`${window.a ? 'user-icon':'btn'} user-icon`} src="https://img3.dian.so/lhc/2023/11/28/132w_132h_4FF241701151209.png" />
        </div>
      </div>
    </div>;
}
export default Footer;