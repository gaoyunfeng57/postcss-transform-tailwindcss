const fs = require('fs')
const path = require('path');
const { test } = require('node:test')
const postcss = require('postcss')
const globs = require('globs');
const sass = require('sass')
const less = require('less')
const postcssNested = require('postcss-nested')
const plugin = require('./')


async function transformSass(from, opts = {}) {
  const dirname = path.dirname(from)
  const basename = path.basename(from);
  const { css } = sass.compile(from);
  const result = await postcss([plugin(opts)]).process(css, { from, to: undefined })
  // fs.createWriteStream(from).write(result.css);
  // equal(result.css, output)
  // equal(result.warnings().length, 0)
}

async function transformCss(from, opts = {}) {
  const dirname = path.dirname(from)
  const basename = path.basename(from);
  const { css } = sass.compile(from);
  const result = await postcss([postcssNested(), plugin(opts)]).process(css, { from, to: undefined })
  // fs.createWriteStream(from).write(result.css);
  // equal(result.css, output)
  // equal(result.warnings().length, 0)
}

async function transformLess(from, opts = {}) {
  const css = fs.readFileSync(from, 'utf-8')
  less.render(css, async (error, output) => {
    if (error) {
      console.warn('error', error)
      return
    }
    const { css } = sass.compileString(output.css);
    const result = await postcss([plugin(opts)]).process(css, { from, to: undefined })
  });
}

test('test transform less', async () => {
  return
  await transformLess('./test/input.less', {
    ratio: 2,
    fileExtensions: ['jsx'],
    removeEmptyClassName: false,
  })
})

/* Write tests here */
test('test transform jsx', async () => {
  return
  await transformCss('./test/input.css', {
    ratio: 2,
    fileExtensions: ['jsx'],
    removeEmptyClassName: false,
  })
})

test('test transform html', async () => {
  return
  await transformCss('./test/input.css', {
    ratio: 2,
    fileExtensions: ['html'],
    removeEmptyClassName: false,
  })
})


test('does test xchargo', () => {
  // transformSass('/Users/xiaodian/Desktop/xchargo/src/pages/orders/list/com/index.powerOrder.scss', {
  //   ratio: 2,
  //   fileExtensions: ['tsx'],
  //   removeEmptyClassName: false,
  //   safeTransform: false,
  //   getClassNames(classNames, merge) {
  //     classNames['align-items'] = merge(classNames['align-items'], {
  //       "align-items: top;": "items-top",
  //       "align-items: bottom;": "items-bottom",
  //     })
  //     return classNames
  //   }
  // })
  // return
  globs('../xchargo/src/pages/**/*.scss', function (err, files) {
    if (err) {
      throw err;
    }
    files.forEach((path) => {
      transformSass(path, {
        ratio: 2,
        fileExtensions: ['tsx'],
        removeEmptyClassName: true,
        getClassNames(classNames, merge) {
          classNames['align-items'] = merge(classNames['align-items'], {
            "align-items: top;": "items-top",
            "align-items: bottom;": "items-bottom",
          })
          return classNames
        }
      })
    });
  });
  globs('../xchargo/src/features/core/components/**/*.scss', function (err, files) {
    if (err) {
      throw err;
    }
    files.forEach((path) => {
      transformSass(path, {
        ratio: 2,
        fileExtensions: ['tsx'],
        removeEmptyClassName: true,
        getClassNames(classNames, merge) {
          classNames['align-items'] = merge(classNames['align-items'], {
            "align-items: top;": "items-top",
            "align-items: bottom;": "items-bottom",
          })
          classNames['animation'] = merge(classNames['animation'], {
            [`animation: fadeDown cubic-bezier(0.86, 0, 0.07, 1) 0.2s forwards;
            @keyframes fadeDown {
              from {
                transform: translate(0, 0);
                opacity: 1;
              }
              to {
                transform: translate(0, 100%);
                opacity: 0;
              }
            }`]: "fadeDown",

            [`animation: fadeUp cubic-bezier(0.86, 0, 0.07, 1) 0.2s forwards;
            @keyframes fadeUp {
              from {
                transform: translate(0, 100%);
                opacity: 0;
              }
              to {
                transform: translate(0, 0);
                opacity: 1;
              }
            }`]: "fadeUp",
          })
          return classNames
        }
      })
    });
  });
  globs('../xchargo/src/components/**/*.scss', function (err, files) {
    if (err) {
      throw err;
    }
    files.forEach((path) => {
      transformSass(path, {
        ratio: 2,
        fileExtensions: ['tsx'],
        removeEmptyClassName: true,
        getClassNames(classNames, merge) {
          classNames['align-items'] = merge(classNames['align-items'], {
            "align-items: top;": "items-top",
            "align-items: bottom;": "items-bottom",
          })
          return classNames
        }
      })
    });
  });
});
