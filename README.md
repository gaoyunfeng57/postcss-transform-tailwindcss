# PostCSS Transform Tailwindcss <img src="https://postcss.github.io/postcss/logo.svg" alt="PostCSS" width="90" height="90" align="right">[postcss]

![Static Badge](https://img.shields.io/badge/node-%3E%3D18.0.0-green.svg?style=flat)
![Static Badge](https://img.shields.io/badge/install%20size-750kb-green.svg?style=flat)
![Static Badge](https://img.shields.io/badge/postcss-%5E8.4.27-green.svg?style=flat)

[PostCSS Transform Tailwindcss] can convert class names from less, sass, and less files to classMame in tsx and class in html

## Quick start

[PostCSS Transform Tailwindcss] is a [PostCSS] plugin.<br>
If you want to transition from semantic class names to Tailwindcss, this is a good tool.

- Install `postcss-transform-tailwindcss` from npm.
- Add `postcss-transform-tailwindcss` to your configuration.
  _[Read more on how to use and install PostCSS Transform Tailwindcss.](#usage)_

## Usage

Add [PostCSS Transform Tailwindcss] to your project:

```bash
npm install postcss-transform-tailwindcss --save-dev
```

Use [PostCSS Transform Tailwindcss] as a [PostCSS] plugin:

### Transform Css

```bash
npm install sass --save-dev
npm install postcss-nested --save-dev
```

```js
const sass = require("sass");
const postcss = require("postcss");
const postcssNested = require("postcss-nested");
const postcssTransformTailwindcss = require("postcss-transform-tailwindcss");
const from = "project/xxx/xxx.css";
const { css } = sass.compile();
postcss([
  postcssNested(),
  postcssTransformTailwindcss(/* pluginOptions */)
]).process(css, {
  from,
  to: undefined
});
```

### Transform Scss

```bash
npm install sass --save-dev
```

```js
const sass = require("sass");
const postcss = require("postcss");
const postcssTransformTailwindcss = require("postcss-transform-tailwindcss");
const from = "project/xxx/xxx.scss";
const { css } = sass.compile();
postcss([postcssTransformTailwindcss(/* pluginOptions */)]).process(css, {
  from,
  to: undefined
});
```

### Transform less

```bash
npm install sass --save-dev
npm install less --save-dev
```

```js
const sass = require("sass");
const postcss = require("postcss");
const postcssTransformTailwindcss = require("postcss-transform-tailwindcss");
const from = "project/xxx/xxx.less";
const css = fs.readFileSync(from, "utf-8");
// https://lesscss.org/usage/
less.render(css, async (error, output) => {
  if (error) {
    console.warn("error", error);
    return;
  }
  const { css } = sass.compileString(output.css);
  const result = await postcss([postcssTransformTailwindcss(opts)]).process(
    css,
    {
      from,
      to: undefined
    }
  );
});
```

### Transform files

```bash
npm install sass --save-dev
npm install less --save-dev
npm install globs --save-dev
```

```js
const sass = require("sass");
const postcss = require("postcss");
const globs = require("globs");
const postcssTransformTailwindcss = require("postcss-transform-tailwindcss");

async function transformSass(from, opts = {}) {
  const { css } = sass.compile(from);
  const result = await postcss([postcssTransformTailwindcss(opts)]).process(
    css,
    {
      from,
      to: undefined
    }
  );
}
globs("xxx/xxx/**/*.scss", function(err, files) {
  if (err) {
    throw err;
  }
  files.forEach(path => {
    transformSass(path, {
      ratio: 2,
      fileExtensions: ["tsx"],
      removeEmptyClassName: true,
      getClassNames(classNames, merge) {
        classNames["align-items"] = merge(classNames["align-items"], {
          "align-items: top;": "items-top",
          "align-items: bottom;": "items-bottom"
        });
        return classNames;
      }
    });
  });
});
```

## Options

### ratio

The ratio option is similar to the ratio of the parameters in the Postcss-pxtransform plugin. For example, if the size is twice the size of the design draft, then ratio: 2.

```js
postcssTransformTailwindcss({ ratio: 0 });
```

### fileExtensions

The FileExtensions option is a collection of files that CSS needs to convert, which can be of file types such as '.jsx ','.tsx', or '.html '

```js
postcssTransformTailwindcss({ fileExtensions: ["jsx"] });
postcssTransformTailwindcss({ fileExtensions: ["jsx", "tsx"] });
postcssTransformTailwindcss({ fileExtensions: ["html", "tsx", "jsx"] });
```

### removeEmptyClassName

The removeEmptyClassName option is very useful during debugging to determine whether to remove the class names of the corresponding. jsx, tsx,. html files that convert CSS

```js
postcssTransformTailwindcss({ removeEmptyClassName: true });
```

### getClassNames

The getClassNames option is a function that takes two parameters. The first parameter is a collection of tailwincss classNames, and the second parameter is [deepmerge]（ https://www.npmjs.com/package/deepmerge ）Method, you need to return the merged classNames

```js
postcssTransformTailwindcss({
  getClassNames(classNames, merge) {
    classNames["align-items"] = merge(classNames["align-items"], {
      "align-items: top;": "items-top",
      "align-items: bottom;": "items-bottom"
    });
    return classNames;
  }
});
```

## Notes

1. When converting HTML files, tag elements must be closed and cannot have comments. At the same time, the label 'DOCTYPE html' needs to be temporarily removed
2. When dealing with. tsx or. jsx.html cross selectors, only a maximum of two will be processed, and nested descendants of cross selectors cannot be processed.
3. Due to the higher priority of the media query selector in tailwindcss compared to regular atomic selectors, the effect is different.
4. When dealing with situations where animations and tailwindcss do not match, you can add custom configurations in tailwind.diag.js and merge custom configurations in the getClassNames plugin
5. Remember to check if the conversion is correct after completion
