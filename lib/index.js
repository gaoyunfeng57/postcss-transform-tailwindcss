const classNames = require("./classNames");
const merge = require("deepmerge");
classNames["border"] = {};
classNames["border-top"] = {};
classNames["border-right"] = {};
classNames["border-bottom"] = {};
classNames["border-left"] = {};
classNames['transform'] = {};
classNames['transition'] = {};
classNames['outline'] = {}
classNames['filter'] = {}
classNames["flex-flow"] = {};
classNames['overflow-y'] = classNames['overflow']
classNames['overflow-x'] = classNames['overflow']
classNames['text-decoration-line'] = classNames['text-decoration']
classNames.top = merge({ "top: 0;": "top-0" }, classNames.top);
classNames.left = merge({ "left: 0;": "left-0" }, classNames.left);
classNames.right = merge({ "right: 0;": "right-0" }, classNames.right);
classNames.bottom = merge({ "bottom: 0;": "bottom-0" }, classNames.bottom);
classNames.flex = merge({ "flex: 1;": "flex-1", "flex: 1 1;": "flex-1" }, classNames.flex);
classNames['letter-spacing'] = merge({ "letter-spacing: 0;": "tracking-normal" }, classNames['letter-spacing']);
classNames["background-color"] = merge(
  {
    "background-color: #FFFFFF;": "bg-white",
    "background-color: #ffffff;": "bg-white",
  },
  classNames["background-color"]
);

classNames["background"] = merge(
  {
    "background: #FFFFFF;": "bg-white",
    "background: #ffffff;": "bg-white",
  },
  classNames["background"]
);

classNames["color"] = merge(
  {
    "color: #FFFFFF;": "text-white",
    "color: #ffffff;": "text-white"
  },
  classNames["color"]
);

classNames["text-decoration"] = merge(
  {
    "text-decoration: underline;": "underline",
    "text-decoration: overline;": "overline",
    "text-decoration: line-through;": "line-through",
    "text-decoration: no-underline;": "no-underline",
  },
  classNames["text-decoration"]
);

classNames["border-color"] = merge(
  {
    "border-color: #FFFFFF;": "border-white",
    "border-color: #ffffff;": "border-white",
    "border-top-color: #FFFFFF;": "border-t-white",
    "border-top-color: #ffffff;": "border-t-white",
    "border-right-color: #FFFFFF;": "border-r-white",
    "border-right-color: #ffffff;": "border-r-white",
    "border-bottom-color: #FFFFFF;": "border-b-white",
    "border-bottom-color: #ffffff;": "border-b-white",
    "border-left-color: #FFFFFF;": "border-l-white",
    "border-left-color: #ffffff;": "border-l-white",
  },
  classNames["border-color"]
);

classNames["font-weight"] = merge(
  {
    "font-weight: normal;": "font-normal",
    "font-weight: bold;": "font-bold",
    "font-weight: bolder;": "font-extrabold",
    "font-weight: lighter;": "font-black",
  },
  classNames["font-weight"]
);


module.exports = classNames;
