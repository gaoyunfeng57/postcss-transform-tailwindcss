exports['accent-color'] = require('./accent-color.js');
exports['align-content'] = require('./align-content.js');
exports['align-items'] = require('./align-items.js');
exports['align-self'] = require('./align-self.js');
exports['animation'] = require('./animation.js');
exports['appearance'] = require('./appearance.js');
exports['aspect-ratio'] = require('./aspect-ratio.js');
exports['backdrop-blur'] = require('./backdrop-blur.js');
exports['backdrop-brightness'] = require('./backdrop-brightness.js');
exports['backdrop-contrast'] = require('./backdrop-contrast.js');
exports['backdrop-grayscale'] = require('./backdrop-grayscale.js');
exports['backdrop-hue-rotate'] = require('./backdrop-hue-rotate.js');
exports['backdrop-invert'] = require('./backdrop-invert.js');
exports['backdrop-opacity'] = require('./backdrop-opacity.js');
exports['backdrop-saturate'] = require('./backdrop-saturate.js');
exports['backdrop-sepia'] = require('./backdrop-sepia.js');
exports['background-attachment'] = require('./background-attachment.js');
exports['background-blend-mode'] = require('./background-blend-mode.js');
exports['background-clip'] = require('./background-clip.js');
exports['background-color'] = require('./background-color.js');
exports['background'] = require('./background-color.js');
exports['background-image'] = require('./background-image.js');
exports['background-origin'] = require('./background-origin.js');
exports['background-position'] = require('./background-position.js');
exports['background-repeat'] = require('./background-repeat.js');
exports['background-size'] = require('./background-size.js');
exports['blur'] = require('./blur.js');
exports['border-collapse'] = require('./border-collapse.js');
exports['border-color'] = require('./border-color.js');
exports['border-radius'] = require('./border-radius.js');
exports["border-start-start-radius"] = require('./border-radius.js');
exports["border-end-start-radius"] = require('./border-radius.js');
exports["border-start-end-radius"] = require('./border-radius.js');
exports["border-end-end-radius"] = require('./border-radius.js');
exports["border-top-left-radius"] = require('./border-radius.js');
exports["border-top-right-radius"] = require('./border-radius.js');
exports["border-bottom-right-radius"] = require('./border-radius.js');
exports["border-bottom-left-radius"] = require('./border-radius.js');
exports['border-spacing'] = require('./border-spacing.js');
exports['border-style'] = require('./border-style.js');
exports['border-width'] = require('./border-width.js');
exports['box-decoration-break'] = require('./box-decoration-break.js');
exports['box-shadow-color'] = require('./box-shadow-color.js');
exports['box-shadow'] = require('./box-shadow.js');
exports['box-sizing'] = require('./box-sizing.js');
exports['break-after'] = require('./break-after.js');
exports['break-before'] = require('./break-before.js');
exports['break-inside'] = require('./break-inside.js');
exports['brightness'] = require('./brightness.js');
exports['caption-side'] = require('./caption-side.js');
exports['caret-color'] = require('./caret-color.js');
exports['clear'] = require('./clear.js');
exports['columns'] = require('./columns.js');
exports['container'] = require('./container.js');
exports['content'] = require('./content.js');
exports['contrast'] = require('./contrast.js');
exports['cursor'] = require('./cursor.js');
exports['display'] = require('./display.js');
exports['divide-color'] = require('./divide-color.js');
exports['divide-style'] = require('./divide-style.js');
exports['divide-width'] = require('./divide-width.js');
exports['drop-shadow'] = require('./drop-shadow.js');
exports['fill'] = require('./fill.js');
exports['flex-basis'] = require('./flex-basis.js');
exports['flex-direction'] = require('./flex-direction.js');
exports['flex-grow'] = require('./flex-grow.js');
exports['flex-shrink'] = require('./flex-shrink.js');
exports['flex-wrap'] = require('./flex-wrap.js');
exports['flex'] = require('./flex.js');
exports['float'] = require('./float.js');
exports['font-family'] = require('./font-family.js');
exports['font-size'] = require('./font-size.js');
exports['font-smoothing'] = require('./font-smoothing.js');
exports['font-style'] = require('./font-style.js');
exports['font-variant-numeric'] = require('./font-variant-numeric.js');
exports['font-weight'] = require('./font-weight.js');
exports['forced-color-adjust'] = require('./forced-color-adjust.js');
exports['gap'] = require('./gap.js');
exports['gradient-color-stops'] = require('./gradient-color-stops.js');
exports['grayscale'] = require('./grayscale.js');
exports['grid-auto-columns'] = require('./grid-auto-columns.js');
exports['grid-auto-flow'] = require('./grid-auto-flow.js');
exports['grid-auto-rows'] = require('./grid-auto-rows.js');
exports['grid-column'] = require('./grid-column.js');
exports['grid-row'] = require('./grid-row.js');
exports['grid-template-columns'] = require('./grid-template-columns.js');
exports['grid-template-rows'] = require('./grid-template-rows.js');
exports['height'] = require('./height.js');
exports['hue-rotate'] = require('./hue-rotate.js');
exports['hyphens'] = require('./hyphens.js');
exports['index'] = require('./index.js');
exports['invert'] = require('./invert.js');
exports['isolation'] = require('./isolation.js');
exports['justify-content'] = require('./justify-content.js');
exports['justify-items'] = require('./justify-items.js');
exports['justify-self'] = require('./justify-self.js');
exports['letter-spacing'] = require('./letter-spacing.js');
exports['line-clamp'] = require('./line-clamp.js');
exports['line-height'] = require('./line-height.js');
exports['list-style-image'] = require('./list-style-image.js');
exports['list-style-position'] = require('./list-style-position.js');
exports['list-style-type'] = require('./list-style-type.js');
exports.margin = require('./margin.js');
exports['margin-top'] = require('./margin.js');
exports['margin-right'] = require('./margin.js');
exports['margin-bottom'] = require('./margin.js');
exports['margin-left'] = require('./margin.js');

exports['max-height'] = require('./max-height.js');
exports['max-width'] = require('./max-width.js');
exports['min-height'] = require('./min-height.js');
exports['min-width'] = require('./min-width.js');
exports['mix-blend-mode'] = require('./mix-blend-mode.js');
exports['object-fit'] = require('./object-fit.js');
exports['object-position'] = require('./object-position.js');
exports['opacity'] = require('./opacity.js');
exports['order'] = require('./order.js');
exports['outline-color'] = require('./outline-color.js');
exports['outline-offset'] = require('./outline-offset.js');
exports['outline-style'] = require('./outline-style.js');
exports['outline-width'] = require('./outline-width.js');
exports['overflow'] = require('./overflow.js');
exports['overscroll-behavior'] = require('./overscroll-behavior.js');
exports['padding'] = require('./padding.js');
exports['padding-left'] = require('./padding.js');
exports['padding-right'] = require('./padding.js');
exports['padding-top'] = require('./padding.js');
exports['padding-bottom'] = require('./padding.js');
exports['place-content'] = require('./place-content.js');
exports['place-items'] = require('./place-items.js');
exports['place-self'] = require('./place-self.js');
exports['pointer-events'] = require('./pointer-events.js');
exports['position'] = require('./position.js');
exports['resize'] = require('./resize.js');
exports['ring-color'] = require('./ring-color.js');
exports['ring-offset-color'] = require('./ring-offset-color.js');
exports['ring-offset-width'] = require('./ring-offset-width.js');
exports['ring-width'] = require('./ring-width.js');
exports['rotate'] = require('./rotate.js');
exports['saturate'] = require('./saturate.js');
exports['scale'] = require('./scale.js');
exports['screen-readers'] = require('./screen-readers.js');
exports['scroll-behavior'] = require('./scroll-behavior.js');
exports['scroll-margin'] = require('./scroll-margin.js');
exports['scroll-padding'] = require('./scroll-padding.js');
exports['scroll-snap-align'] = require('./scroll-snap-align.js');
exports['scroll-snap-stop'] = require('./scroll-snap-stop.js');
exports['scroll-snap-type'] = require('./scroll-snap-type.js');
exports['sepia'] = require('./sepia.js');
exports['size'] = require('./size.js');
exports['skew'] = require('./skew.js');
exports['space'] = require('./space.js');
exports['stroke-width'] = require('./stroke-width.js');
exports['stroke'] = require('./stroke.js');
exports['table-layout'] = require('./table-layout.js');
exports['text-align'] = require('./text-align.js');
exports['color'] = require('./text-color.js');
exports['text-decoration-color'] = require('./text-decoration-color.js');
exports['text-decoration-style'] = require('./text-decoration-style.js');
exports['text-decoration-thickness'] = require('./text-decoration-thickness.js');
exports['text-decoration'] = require('./text-decoration.js');
exports['text-indent'] = require('./text-indent.js');
exports['text-overflow'] = require('./text-overflow.js');
exports['text-transform'] = require('./text-transform.js');
exports['text-underline-offset'] = require('./text-underline-offset.js');
exports['text-wrap'] = require('./text-wrap.js');
exports.top = require('./top-right-bottom-left.js');
exports.right = require('./top-right-bottom-left.js');
exports.bottom = require('./top-right-bottom-left.js');
exports.left = require('./top-right-bottom-left.js');
exports['touch-action'] = require('./touch-action.js');
exports['transform-origin'] = require('./transform-origin.js');
exports['transition-delay'] = require('./transition-delay.js');
exports['transition-duration'] = require('./transition-duration.js');
exports['transition-property'] = require('./transition-property.js');
exports['transition-timing-function'] = require('./transition-timing-function.js');
exports['translate'] = require('./translate.js');
exports['user-select'] = require('./user-select.js');
exports['vertical-align'] = require('./vertical-align.js');
exports['visibility'] = require('./visibility.js');
exports['white-space'] = require('./whitespace.js');
exports['width'] = require('./width.js');
exports['will-change'] = require('./will-change.js');
exports['word-break'] = require('./word-break.js');
exports['z-index'] = require('./z-index.js');
exports['hover-focus-and-other-states'] = require('./hover-focus-and-other-states.js');
exports['responsive-design'] = require('./responsive-design')