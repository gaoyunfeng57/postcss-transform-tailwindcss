module.exports = {
  "border-radius: 0px;": "rounded-none",
  "border-radius: 0.125rem; /* 2px */": "rounded-sm",
  "border-radius: 0.25rem; /* 4px */": "rounded",
  "border-radius: 0.375rem; /* 6px */": "rounded-md",
  "border-radius: 0.5rem; /* 8px */": "rounded-lg",
  "border-radius: 0.75rem; /* 12px */": "rounded-xl",
  "border-radius: 1rem; /* 16px */": "rounded-2xl",
  "border-radius: 1.5rem; /* 24px */": "rounded-3xl",
  "border-radius: 9999px;": "rounded-full",
  "border-start-start-radius: 0px;\nborder-end-start-radius: 0px;":
    "rounded-s-none",
  "border-start-start-radius: 0.125rem; /* 2px */\nborder-end-start-radius: 0.125rem; /* 2px */":
    "rounded-s-sm",
  "border-start-start-radius: 0.25rem; /* 4px */\nborder-end-start-radius: 0.25rem; /* 4px */":
    "rounded-s",
  "border-start-start-radius: 0.375rem; /* 6px */\nborder-end-start-radius: 0.375rem; /* 6px */":
    "rounded-s-md",
  "border-start-start-radius: 0.5rem; /* 8px */\nborder-end-start-radius: 0.5rem; /* 8px */":
    "rounded-s-lg",
  "border-start-start-radius: 0.75rem; /* 12px */\nborder-end-start-radius: 0.75rem; /* 12px */":
    "rounded-s-xl",
  "border-start-start-radius: 1rem; /* 16px */\nborder-end-start-radius: 1rem; /* 16px */":
    "rounded-s-2xl",
  "border-start-start-radius: 1.5rem; /* 24px */\nborder-end-start-radius: 1.5rem; /* 24px */":
    "rounded-s-3xl",
  "border-start-start-radius: 9999px;\nborder-end-start-radius: 9999px;":
    "rounded-s-full",
  "border-start-end-radius: 0px;\nborder-end-end-radius: 0px;":
    "rounded-e-none",
  "border-start-end-radius: 0.125rem; /* 2px */\nborder-end-end-radius: 0.125rem; /* 2px */":
    "rounded-e-sm",
  "border-start-end-radius: 0.25rem; /* 4px */\nborder-end-end-radius: 0.25rem; /* 4px */":
    "rounded-e",
  "border-start-end-radius: 0.375rem; /* 6px */\nborder-end-end-radius: 0.375rem; /* 6px */":
    "rounded-e-md",
  "border-start-end-radius: 0.5rem; /* 8px */\nborder-end-end-radius: 0.5rem; /* 8px */":
    "rounded-e-lg",
  "border-start-end-radius: 0.75rem; /* 12px */\nborder-end-end-radius: 0.75rem; /* 12px */":
    "rounded-e-xl",
  "border-start-end-radius: 1rem; /* 16px */\nborder-end-end-radius: 1rem; /* 16px */":
    "rounded-e-2xl",
  "border-start-end-radius: 1.5rem; /* 24px */\nborder-end-end-radius: 1.5rem; /* 24px */":
    "rounded-e-3xl",
  "border-start-end-radius: 9999px;\nborder-end-end-radius: 9999px;":
    "rounded-e-full",
  "border-top-left-radius: 0px;\nborder-top-right-radius: 0px;":
    "rounded-t-none",
  "border-top-left-radius: 0.125rem; /* 2px */\nborder-top-right-radius: 0.125rem; /* 2px */":
    "rounded-t-sm",
  "border-top-left-radius: 0.25rem; /* 4px */\nborder-top-right-radius: 0.25rem; /* 4px */":
    "rounded-t",
  "border-top-left-radius: 0.375rem; /* 6px */\nborder-top-right-radius: 0.375rem; /* 6px */":
    "rounded-t-md",
  "border-top-left-radius: 0.5rem; /* 8px */\nborder-top-right-radius: 0.5rem; /* 8px */":
    "rounded-t-lg",
  "border-top-left-radius: 0.75rem; /* 12px */\nborder-top-right-radius: 0.75rem; /* 12px */":
    "rounded-t-xl",
  "border-top-left-radius: 1rem; /* 16px */\nborder-top-right-radius: 1rem; /* 16px */":
    "rounded-t-2xl",
  "border-top-left-radius: 1.5rem; /* 24px */\nborder-top-right-radius: 1.5rem; /* 24px */":
    "rounded-t-3xl",
  "border-top-left-radius: 9999px;\nborder-top-right-radius: 9999px;":
    "rounded-t-full",
  "border-top-right-radius: 0px;\nborder-bottom-right-radius: 0px;":
    "rounded-r-none",
  "border-top-right-radius: 0.125rem; /* 2px */\nborder-bottom-right-radius: 0.125rem; /* 2px */":
    "rounded-r-sm",
  "border-top-right-radius: 0.25rem; /* 4px */\nborder-bottom-right-radius: 0.25rem; /* 4px */":
    "rounded-r",
  "border-top-right-radius: 0.375rem; /* 6px */\nborder-bottom-right-radius: 0.375rem; /* 6px */":
    "rounded-r-md",
  "border-top-right-radius: 0.5rem; /* 8px */\nborder-bottom-right-radius: 0.5rem; /* 8px */":
    "rounded-r-lg",
  "border-top-right-radius: 0.75rem; /* 12px */\nborder-bottom-right-radius: 0.75rem; /* 12px */":
    "rounded-r-xl",
  "border-top-right-radius: 1rem; /* 16px */\nborder-bottom-right-radius: 1rem; /* 16px */":
    "rounded-r-2xl",
  "border-top-right-radius: 1.5rem; /* 24px */\nborder-bottom-right-radius: 1.5rem; /* 24px */":
    "rounded-r-3xl",
  "border-top-right-radius: 9999px;\nborder-bottom-right-radius: 9999px;":
    "rounded-r-full",
  "border-bottom-right-radius: 0px;\nborder-bottom-left-radius: 0px;":
    "rounded-b-none",
  "border-bottom-right-radius: 0.125rem; /* 2px */\nborder-bottom-left-radius: 0.125rem; /* 2px */":
    "rounded-b-sm",
  "border-bottom-right-radius: 0.25rem; /* 4px */\nborder-bottom-left-radius: 0.25rem; /* 4px */":
    "rounded-b",
  "border-bottom-right-radius: 0.375rem; /* 6px */\nborder-bottom-left-radius: 0.375rem; /* 6px */":
    "rounded-b-md",
  "border-bottom-right-radius: 0.5rem; /* 8px */\nborder-bottom-left-radius: 0.5rem; /* 8px */":
    "rounded-b-lg",
  "border-bottom-right-radius: 0.75rem; /* 12px */\nborder-bottom-left-radius: 0.75rem; /* 12px */":
    "rounded-b-xl",
  "border-bottom-right-radius: 1rem; /* 16px */\nborder-bottom-left-radius: 1rem; /* 16px */":
    "rounded-b-2xl",
  "border-bottom-right-radius: 1.5rem; /* 24px */\nborder-bottom-left-radius: 1.5rem; /* 24px */":
    "rounded-b-3xl",
  "border-bottom-right-radius: 9999px;\nborder-bottom-left-radius: 9999px;":
    "rounded-b-full",
  "border-top-left-radius: 0px;\nborder-bottom-left-radius: 0px;":
    "rounded-l-none",
  "border-top-left-radius: 0.125rem; /* 2px */\nborder-bottom-left-radius: 0.125rem; /* 2px */":
    "rounded-l-sm",
  "border-top-left-radius: 0.25rem; /* 4px */\nborder-bottom-left-radius: 0.25rem; /* 4px */":
    "rounded-l",
  "border-top-left-radius: 0.375rem; /* 6px */\nborder-bottom-left-radius: 0.375rem; /* 6px */":
    "rounded-l-md",
  "border-top-left-radius: 0.5rem; /* 8px */\nborder-bottom-left-radius: 0.5rem; /* 8px */":
    "rounded-l-lg",
  "border-top-left-radius: 0.75rem; /* 12px */\nborder-bottom-left-radius: 0.75rem; /* 12px */":
    "rounded-l-xl",
  "border-top-left-radius: 1rem; /* 16px */\nborder-bottom-left-radius: 1rem; /* 16px */":
    "rounded-l-2xl",
  "border-top-left-radius: 1.5rem; /* 24px */\nborder-bottom-left-radius: 1.5rem; /* 24px */":
    "rounded-l-3xl",
  "border-top-left-radius: 9999px;\nborder-bottom-left-radius: 9999px;":
    "rounded-l-full",
  "border-start-start-radius: 0px;": "rounded-ss-none",
  "border-start-start-radius: 0.125rem; /* 2px */": "rounded-ss-sm",
  "border-start-start-radius: 0.25rem; /* 4px */": "rounded-ss",
  "border-start-start-radius: 0.375rem; /* 6px */": "rounded-ss-md",
  "border-start-start-radius: 0.5rem; /* 8px */": "rounded-ss-lg",
  "border-start-start-radius: 0.75rem; /* 12px */": "rounded-ss-xl",
  "border-start-start-radius: 1rem; /* 16px */": "rounded-ss-2xl",
  "border-start-start-radius: 1.5rem; /* 24px */": "rounded-ss-3xl",
  "border-start-start-radius: 9999px;": "rounded-ss-full",
  "border-start-end-radius: 0px;": "rounded-se-none",
  "border-start-end-radius: 0.125rem; /* 2px */": "rounded-se-sm",
  "border-start-end-radius: 0.25rem; /* 4px */": "rounded-se",
  "border-start-end-radius: 0.375rem; /* 6px */": "rounded-se-md",
  "border-start-end-radius: 0.5rem; /* 8px */": "rounded-se-lg",
  "border-start-end-radius: 0.75rem; /* 12px */": "rounded-se-xl",
  "border-start-end-radius: 1rem; /* 16px */": "rounded-se-2xl",
  "border-start-end-radius: 1.5rem; /* 24px */": "rounded-se-3xl",
  "border-start-end-radius: 9999px;": "rounded-se-full",
  "border-end-end-radius: 0px;": "rounded-ee-none",
  "border-end-end-radius: 0.125rem; /* 2px */": "rounded-ee-sm",
  "border-end-end-radius: 0.25rem; /* 4px */": "rounded-ee",
  "border-end-end-radius: 0.375rem; /* 6px */": "rounded-ee-md",
  "border-end-end-radius: 0.5rem; /* 8px */": "rounded-ee-lg",
  "border-end-end-radius: 0.75rem; /* 12px */": "rounded-ee-xl",
  "border-end-end-radius: 1rem; /* 16px */": "rounded-ee-2xl",
  "border-end-end-radius: 1.5rem; /* 24px */": "rounded-ee-3xl",
  "border-end-end-radius: 9999px;": "rounded-ee-full",
  "border-end-start-radius: 0px;": "rounded-es-none",
  "border-end-start-radius: 0.125rem; /* 2px */": "rounded-es-sm",
  "border-end-start-radius: 0.25rem; /* 4px */": "rounded-es",
  "border-end-start-radius: 0.375rem; /* 6px */": "rounded-es-md",
  "border-end-start-radius: 0.5rem; /* 8px */": "rounded-es-lg",
  "border-end-start-radius: 0.75rem; /* 12px */": "rounded-es-xl",
  "border-end-start-radius: 1rem; /* 16px */": "rounded-es-2xl",
  "border-end-start-radius: 1.5rem; /* 24px */": "rounded-es-3xl",
  "border-end-start-radius: 9999px;": "rounded-es-full",
  "border-top-left-radius: 0px;": "rounded-tl-none",
  "border-top-left-radius: 0.125rem; /* 2px */": "rounded-tl-sm",
  "border-top-left-radius: 0.25rem; /* 4px */": "rounded-tl",
  "border-top-left-radius: 0.375rem; /* 6px */": "rounded-tl-md",
  "border-top-left-radius: 0.5rem; /* 8px */": "rounded-tl-lg",
  "border-top-left-radius: 0.75rem; /* 12px */": "rounded-tl-xl",
  "border-top-left-radius: 1rem; /* 16px */": "rounded-tl-2xl",
  "border-top-left-radius: 1.5rem; /* 24px */": "rounded-tl-3xl",
  "border-top-left-radius: 9999px;": "rounded-tl-full",
  "border-top-right-radius: 0px;": "rounded-tr-none",
  "border-top-right-radius: 0.125rem; /* 2px */": "rounded-tr-sm",
  "border-top-right-radius: 0.25rem; /* 4px */": "rounded-tr",
  "border-top-right-radius: 0.375rem; /* 6px */": "rounded-tr-md",
  "border-top-right-radius: 0.5rem; /* 8px */": "rounded-tr-lg",
  "border-top-right-radius: 0.75rem; /* 12px */": "rounded-tr-xl",
  "border-top-right-radius: 1rem; /* 16px */": "rounded-tr-2xl",
  "border-top-right-radius: 1.5rem; /* 24px */": "rounded-tr-3xl",
  "border-top-right-radius: 9999px;": "rounded-tr-full",
  "border-bottom-right-radius: 0px;": "rounded-br-none",
  "border-bottom-right-radius: 0.125rem; /* 2px */": "rounded-br-sm",
  "border-bottom-right-radius: 0.25rem; /* 4px */": "rounded-br",
  "border-bottom-right-radius: 0.375rem; /* 6px */": "rounded-br-md",
  "border-bottom-right-radius: 0.5rem; /* 8px */": "rounded-br-lg",
  "border-bottom-right-radius: 0.75rem; /* 12px */": "rounded-br-xl",
  "border-bottom-right-radius: 1rem; /* 16px */": "rounded-br-2xl",
  "border-bottom-right-radius: 1.5rem; /* 24px */": "rounded-br-3xl",
  "border-bottom-right-radius: 9999px;": "rounded-br-full",
  "border-bottom-left-radius: 0px;": "rounded-bl-none",
  "border-bottom-left-radius: 0.125rem; /* 2px */": "rounded-bl-sm",
  "border-bottom-left-radius: 0.25rem; /* 4px */": "rounded-bl",
  "border-bottom-left-radius: 0.375rem; /* 6px */": "rounded-bl-md",
  "border-bottom-left-radius: 0.5rem; /* 8px */": "rounded-bl-lg",
  "border-bottom-left-radius: 0.75rem; /* 12px */": "rounded-bl-xl",
  "border-bottom-left-radius: 1rem; /* 16px */": "rounded-bl-2xl",
  "border-bottom-left-radius: 1.5rem; /* 24px */": "rounded-bl-3xl",
  "border-bottom-left-radius: 9999px;": "rounded-bl-full",
};
