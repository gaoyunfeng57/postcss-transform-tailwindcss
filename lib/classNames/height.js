module.exports = {
  "height: 0px;": "h-0",
  "height: 1px;": "h-px",
  "height: 0.125rem; /* 2px */": "h-0.5",
  "height: 0.25rem; /* 4px */": "h-1",
  "height: 0.375rem; /* 6px */": "h-1.5",
  "height: 0.5rem; /* 8px */": "h-2",
  "height: 0.625rem; /* 10px */": "h-2.5",
  "height: 0.75rem; /* 12px */": "h-3",
  "height: 0.875rem; /* 14px */": "h-3.5",
  "height: 1rem; /* 16px */": "h-4",
  "height: 1.25rem; /* 20px */": "h-5",
  "height: 1.5rem; /* 24px */": "h-6",
  "height: 1.75rem; /* 28px */": "h-7",
  "height: 2rem; /* 32px */": "h-8",
  "height: 2.25rem; /* 36px */": "h-9",
  "height: 2.5rem; /* 40px */": "h-10",
  "height: 2.75rem; /* 44px */": "h-11",
  "height: 3rem; /* 48px */": "h-12",
  "height: 3.5rem; /* 56px */": "h-14",
  "height: 4rem; /* 64px */": "h-16",
  "height: 5rem; /* 80px */": "h-20",
  "height: 6rem; /* 96px */": "h-24",
  "height: 7rem; /* 112px */": "h-28",
  "height: 8rem; /* 128px */": "h-32",
  "height: 9rem; /* 144px */": "h-36",
  "height: 10rem; /* 160px */": "h-40",
  "height: 11rem; /* 176px */": "h-44",
  "height: 12rem; /* 192px */": "h-48",
  "height: 13rem; /* 208px */": "h-52",
  "height: 14rem; /* 224px */": "h-56",
  "height: 15rem; /* 240px */": "h-60",
  "height: 16rem; /* 256px */": "h-64",
  "height: 18rem; /* 288px */": "h-72",
  "height: 20rem; /* 320px */": "h-80",
  "height: 24rem; /* 384px */": "h-96",
  "height: auto;": "h-auto",
  "height: 50%;": "h-3/6",
  "height: 33.333333%;": "h-2/6",
  "height: 66.666667%;": "h-4/6",
  "height: 25%;": "h-1/4",
  "height: 75%;": "h-3/4",
  "height: 20%;": "h-1/5",
  "height: 40%;": "h-2/5",
  "height: 60%;": "h-3/5",
  "height: 80%;": "h-4/5",
  "height: 16.666667%;": "h-1/6",
  "height: 83.333333%;": "h-5/6",
  "height: 100%;": "h-full",
  "height: 100vh;": "h-screen",
  "height: 100svh;": "h-svh",
  "height: 100lvh;": "h-lvh",
  "height: 100dvh;": "h-dvh",
  "height: min-content;": "h-min",
  "height: max-content;": "h-max",
  "height: fit-content;": "h-fit"
};
