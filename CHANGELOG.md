# Changelog

All notable changes to this project will be documented in this file. See [standard-version](https://github.com/conventional-changelog/standard-version) for commit guidelines.

### [1.1.4](https://gitee.com/gaoyunfeng57/postcss-transform-tailwindcss/compare/v1.1.3...v1.1.4) (2024-07-30)

### [1.1.3](https://gitee.com/gaoyunfeng57/postcss-transform-tailwindcss/compare/v1.1.2...v1.1.3) (2024-07-24)

### [1.1.2](https://gitee.com/gaoyunfeng57/postcss-transform-tailwindcss/compare/v1.1.1...v1.1.2) (2024-07-23)

### [1.1.1](https://gitee.com/gaoyunfeng57/postcss-transform-tailwindcss/compare/v1.1.0...v1.1.1) (2024-07-23)

## 1.1.0 (2024-07-23)


### Features

* init ([fa78cd4](https://gitee.com/gaoyunfeng57/postcss-transform-tailwindcss/commit/fa78cd4843a03c55e6af01b3c39f7100fe112c1f))
